#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import telegram as tg
import re
from phrases import get_phrase_for
from userdb import userdb

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

HANDLE = range(1)
# ALIGN_GROUP_ID = -1001229130034
DB_FILE = 'users.json'
TOKEN = "672992864:AAF4wfrcbXo6J1pkqGaO8wy3vVBD0KUwOLc"


# BOT = tg.Bot(TOKEN)
# ALIGN_GROUP = BOT.getChat(ALIGN_GROUP_ID)
UDB = userdb(DB_FILE)


# TODO: switch to context
def start(bot, update):
    # reply_keyboard = [['Хочу выравниться']]
    print(update.message.chat_id)

    update.message.reply_text(
        'Заступаю на дежурство!') #,
        # reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False))

    return HANDLE


# def invite(update):
#     update.message.reply_text(
#         "Отлично! Но тут такое дело, придётся тебе зайти сначала самому:\n\n" + 
#         "{}\n\n".format(ALIGN_GROUP.link) +
#         "Если тебя не будут выравнивать - жалуйся @mi_volodin"
#     )

# TODO: switch to context
def help(bot, update):
    update.message.reply_text(
        "Да всё просто.\n"
        "Если вдруг увижу, что кто-то хочет ВЫРАВНиваться"
        " или что-то подобное - я сразу реагирую и помогаю"
        " достичь этой цели.\n"
    )
    return 0


def get_mention(user):
    if user.username is not None:
        return user.mention_html(
            name=(u"@" + user.username))
    return user.mention_html()


def handle(bot, update):
    # reply = update.message.text
    user = update.message.from_user
    chat = update.message.chat

    # if not (reply == 'Хочу выравниться'):
    #     return HANDLE

    logging.debug('Handle start')
    if not UDB.is_in(user.id):
        UDB.push(user.id, user.username, user.first_name, user.last_name)

    if chat.type == 'private':
        update.message.reply_text(
            'Прости, братан, я умею выравнивать только в группах.\n'
            'Добавь меня, если хочешь куда-нибудь.'
        )
        return 0

    if not UDB.has_chat(chat.id):
        UDB.add_chat(chat.id, scan_chat(bot, update))

    # if chat.get_member(user.id) is None:
    #     update.message.reply_text(
    #         "Отлично! Но тут такое дело, придётся тебе зайти сначала самому:\n\n" + 
    #         "{}\n\n".format(ALIGN_GROUP.link) +
    #         "Если тебя не будут выравнивать - жалуйся @mi_volodin"
    #     )
    #     return 0   

    user_mention = get_mention(user)
    # print(user_mention)
    # print(user.mention_html(user.username))

    in_chat = 0
    if UDB.has_chat(chat.id):
        in_chat = chat.id

    user2_id = UDB.get_random_id(except_id=user.id, in_chat=in_chat)
    if user2_id == 0:
        update.message.reply_html(get_phrase_for(1))
        return 0

    user2_mention = get_mention(chat.get_member(user2_id).user)
    logging.debug('Handle reply')

    update.message.reply_html(
        get_phrase_for(2).format(user_mention, user2_mention)
    )

    return 0


def ls(bot, update):
    if UDB.is_in(update.message.from_user.id):
        update.message.reply_text(
            'Я сейчас умею выравнивать вот этих людей\n' +
            ('\n'.join(UDB.list()))
        )
    else:
        update.message.reply_text('Братан, я тебя ещё не знаю. Попроси повыравниваться.')

    return 0


def ls_chat(bot, update):
    if UDB.is_in(update.message.from_user.id):
        chat_id = update.message.chat.id
        if not UDB.has_chat(chat_id):
            UDB.add_chat(chat_id, scan_chat(bot, update))
        update.message.reply_text(
            'Я сейчас умею выравнивать вот этих людей в этом чате\n' +
            ('\n'.join(UDB.list_inchat(chat_id)))
        )
    else:
        update.message.reply_text('Братан, я тебя ещё не знаю. Попроси повыравниваться.')

    return 0

# def cancel(bot, update):
#     user = update.message.from_user
#     logger.info("User %s canceled the conversation.", user.first_name)
#     update.message.reply_text('Ну ок, увидимся позже.',
#                               reply_markup=ReplyKeyboardRemove())

#     return ConversationHandler.END

def auto_add(bot, update):
    users = update.message.new_chat_members
    chat_id = update.message.chat.id

    for u in users:
        print(u.id)
        UDB.push(u.id, u.username, u.first_name, u.last_name)
        UDB.add_user_to_chat(chat_id, u.id)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def scan_chat(bot, update):
    chat_id = update.message.chat.id

    allu = UDB.get_all_user_ids()
    # chatu = bot.get_chat_memebers_count(chat_id)

    users_in_chat = []
    for u in allu:
        try:
            res = bot.get_chat_member(chat_id, u)
            # print(res)
            if res['status'] != 'left':
                users_in_chat.append(u)
        except tg.TelegramError as e:
            pass

    return users_in_chat


def whatsnew(bot, update):
    with open('changelog.txt', 'r', encoding='utf-8') as f:
        log = f.read()
        update.message.reply_html(log)
    return


def main():
    # Create the EventHandler and pass it your bot's token.
    # logging.getLogger().setLevel(logging.DEBUG)
    updater = Updater("672992864:AAF4wfrcbXo6J1pkqGaO8wy3vVBD0KUwOLc")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    rec = re.compile(r'^[\w\W]*(выр[ао]вн)', re.I | re.M)
    # rec2 = re.compile(r'^[\w\W]*( ЗНИ[,\.\;])', re.I | re.M)

    entry_handler = MessageHandler(
        Filters.status_update.new_chat_members, auto_add)

    # adding_handler = MessageHandler(
    #     Filters.username)

    # dp.add_handler(conv_handler)
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('list', ls))
    dp.add_handler(CommandHandler('listchat', ls_chat))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(CommandHandler('whatsnew', whatsnew))
    dp.add_handler(RegexHandler(rec, handle))
    # dp.add_handler(RegexHandler(rec2, handle))
    dp.add_handler(entry_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
    # print(ALIGN_GROUP.link)
    # print('Current users:\n' + '\n'.join(UDB.list()))