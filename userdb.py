import json
import random


class userdb:

    DB = dict()
    CHATS = dict()
    db_file = None

    def __init__(self, file):
        with open(file) as f:
            CURDB = json.load(f)

        self.DB = {int(k): v for k, v in CURDB.items()}
        self.db_file = file
        return

    def __enter__(self):
        pass

    def dump(self):
        with open(self.db_file, 'w') as f:
            json.dump(self.DB, f)

    def __exit__(self, exc_type, exc_value, traceback):
        self.dump()

    def is_in(self, id):
        return (id in self.DB)

    def push(self, id, username, first_name, last_name):
        if id in self.DB:
            return -1
        else:
            self.DB[id] = {
                'username': username,
                'first_name': first_name,
                'last_name': last_name
            }
            self.dump()

        return 0

    def list(self):
        return [
            '@{}'.format(e['username'])
            if e['username'] is not None
                and len(e['username']) > 1
            else '{} {}'.format(e['first_name'], e['last_name'])
            for k, e in self.DB.items()
        ]

    def list_inchat(self, chat_id):
        return [
            '@{}'.format(e['username'])
            if e['username'] is not None
                and len(e['username']) > 1
            else '{} {}'.format(e['first_name'], e['last_name'])
            for k, e in self.DB.items()
            if k in self.CHATS[chat_id]
        ]

    def pop(self, user):
        pass


    def get_random_id(self, except_id, in_chat=0):
        """
        Возвращает рандомного пользователя, исключая пользователя с except_id.
        Если указан чат, ищет только по чату.
        """

        if in_chat != 0:
            userids = [k for k in self.CHATS[in_chat] if k != except_id]
        else:
            userids = [k for k in self.DB.keys() if k != except_id]
        # userids.remove(except_id)

        if len(userids) == 0:
            return 0

        pick_id = random.choice(userids)

        return int(pick_id)

    def add_chat(self, chat_id, user_list):
        """
        Добавляет чат в библиотеку чатов, чтобы можно было пользователей привязывать к чатам.
        Чаты не сохраняются, а инициализируются потихоньку, когда идёт обращение в них.
        """
        self.CHATS[chat_id] = user_list
        return

    def has_chat(self, chat_id):
        return chat_id in self.CHATS

    def get_all_user_ids(self):
        return [k for k in self.DB.keys()]

    def add_user_to_chat(self, chat_ids, user_id):
        ids = chat_ids
        if isinstance(chat_ids, int):
            ids = [chat_ids]
        for c in chat_ids:
            self.CHATS[c].append(user_id)
        return


if __name__ == '__main__':
    DB_FILE = 'users.json'
    udb = userdb(DB_FILE)
    print(udb.list())
